<html> <body> <h1>Data Students</h1>
@if(!empty($data))
    <table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>firstname</th>
        <th>surname</th>
        <th>email</th>
        <th>nationality</th>
        <th>Address</th>
        <th>University</th>
        <th>Course</th>
    </tr>
    </thead>
    @foreach ($data as $row)
            <tr>
                <td>{{$row['id']}}</td>
                <td>{{$row['firstname']}}</td>
                <td>{{$row['surname']}}</td>
                <td>{{$row['email']}}</td>
                <td>{{$row['nationality']}}</td>
                <td>{{$row['address']['houseNo'] . " " . $row['address']['line_1'] . ", " . $row['address']['city']}}</td>
                <td>{{$row['course']['university']}}</td>
                <td>{{$row['course']['course_name']}}</td>
            </tr>

    @endforeach </table> @endif
</body>
</html>