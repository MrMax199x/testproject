<html> <body> <h1>Data Courses</h1>
@if(!empty($data))
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>university</th>
            <th>course name</th>
            <th>count of student</th>
        </tr>
        </thead>
        @foreach ($data as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->university}}</td>
                <td>{{$row->course_name}}</td>
                <td>{{$row->students->count()}}</td>
            </tr>

        @endforeach </table> @endif
</body>
</html>