<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Students;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course')->get();
        return view('view_students', compact(['students']));
    }

    public function export(Request $request)
    {
        $id = explode(',', $request->input('id'));
        $students = Students::whereIn('id', $id)->with(['course', 'address'])->get();
        $this->createFile('Students', 'students', $students)->export('csv');
    }

    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV()
    {
        $students = Students::with(['course', 'address'])->get();
        $this->createFile('Students', 'students', $students)->export('csv');
    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exporttCourseAttendenceToCSV()
    {
        $courses = Course::with('students')->get();
        $this->createFile('Courses', 'courses', $courses)->export('csv');
    }

    protected function createFile($title, $view, $data)
    {
        return Excel::create($title, function($excel) use($view, $data) {
            $excel->sheet('Sheet 1', function($sheet) use($view, $data) {
                $sheet->loadView($view, compact(['data']));
            });
        });
    }
}
