<?php
/**
 * Created by PhpStorm.
 * User: kuendo
 * Date: 19.08.17
 * Time: 16:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAddresses extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_address';

    public $timestamps = false;

    public function students()
    {
        return $this->hasMany('App\Models\Students');
    }

    public function getFullAddress()
    {
        return $this->houseNo . " " . $this->line_1 . ", " . $this->city;
    }
}